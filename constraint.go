package update_server

import (
	"errors"

	"github.com/coreos/go-semver/semver"
)

type Operator byte

const (
	/* operator integer values */
	OP_WRONG Operator = iota
	OP_EQ
	OP_LT
	OP_LE
	OP_GT
	OP_GE
	OP_NE
)

func MakeOperator(value string) (Operator, error) {

	switch value {
	case "==", "=":
		return OP_EQ, nil
	case "<":
		return OP_LT, nil
	case "<=":
		return OP_LE, nil
	case ">":
		return OP_GT, nil
	case ">=":
		return OP_GE, nil
	case "!=", "<>":
		return OP_NE, nil
	default:
		return OP_WRONG, errors.New("Wrong operator")
	}
}

func (o Operator) String() string {
	switch o {
	case OP_EQ:
		return "=="
	case OP_LT:
		return "<"
	case OP_LE:
		return "<="
	case OP_GT:
		return ">"
	case OP_GE:
		return ">="
	case OP_NE:
		return "!="
	}
	return "unknown"
}

type Constraint struct {
	Operator Operator
	Version  semver.Version
}

func NewConstraint(operatorStr string, versionStr string) (*Constraint, error) {
	version, err := semver.NewVersion(versionStr)
	if err != nil {
		return nil, err
	}

	operator, err := MakeOperator(operatorStr)
	if err != nil {
		return nil, err
	}

	return &Constraint{operator, *version}, nil
}

func (this *Constraint) Matches(version string) bool {

	vAnother := semver.New(version)

	switch this.Operator {
	case OP_EQ:
		return vAnother.Equal(this.Version)
	case OP_LT:
		return vAnother.LessThan(this.Version) && !vAnother.Equal(this.Version)
	case OP_LE:
		return vAnother.LessThan(this.Version) || vAnother.Equal(this.Version)
	case OP_GT:
		return !vAnother.LessThan(this.Version) && !vAnother.Equal(this.Version)
	case OP_GE:
		return !vAnother.LessThan(this.Version) || vAnother.Equal(this.Version)
	case OP_NE:
		return !vAnother.Equal(this.Version)
	}

	return false
}

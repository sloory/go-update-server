package update_server

import (
	"fmt"

	"github.com/coreos/go-semver/semver"
)

func main() {
	vA := semver.New("1.2.3")
	vB := semver.New("3.2.1")

	fmt.Printf("%s < %s == %t\n", vA, vB, vA.LessThan(*vB))

}

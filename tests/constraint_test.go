package update_server

import (
	"testing"

	update_server "gitlab.com/sloory/go-update-server"
)

type fixture struct {
	ProvideVersion  string
	RequireOperator string
	RequireVersion  string
}

var failing_fixtures = []fixture{
	{"2.0.0", "==", "1.0.0"},
	{"2.0.0", "==", "3.0.0"},

	{"2.0.0", "<", "2.0.0"},
	{"2.0.0", "<", "1.0.0"},

	{"2.0.0", "<=", "1.0.0"},

	{"2.0.0", ">=", "3.0.0"},

	{"2.0.0", ">", "3.0.0"},
	{"2.0.0", ">", "2.0.0"},

	{"2.0.0", "!=", "2.0.0"},
}

func TestFailedMatches(t *testing.T) {
	for _, v := range failing_fixtures {

		versionRequire, err := update_server.NewConstraint(
			v.RequireOperator,
			v.RequireVersion,
		)
		if err != nil {
			panic(err)
		}

		if versionRequire.Matches(v.ProvideVersion) {
			t.Errorf("%s should not match %s", v.ProvideVersion, versionRequire)
		}
	}
}

var success_fixtures = []fixture{
	{"2.0.0", "==", "2.0.0"},

	{"2.0.0", "<", "3.0.0"},

	{"2.0.0", "<=", "2.0.0"},

	{"2.0.0", ">=", "2.0.0"},
	{"2.0.0", ">=", "1.0.0"},

	{"2.0.0", ">", "1.0.0"},

	{"2.0.0", "!=", "1.0.0"},
	{"2.0.0", "!=", "3.0.0"},
}

func TestSuccessMatches(t *testing.T) {
	for _, v := range success_fixtures {

		versionRequire, err := update_server.NewConstraint(
			v.RequireOperator,
			v.RequireVersion,
		)

		if err != nil {
			panic(err)
		}

		if !versionRequire.Matches(v.ProvideVersion) {
			t.Errorf("%s should match %s ", v.ProvideVersion, versionRequire)
		}
	}
}
